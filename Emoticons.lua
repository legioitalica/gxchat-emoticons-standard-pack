-- Author      : Galilman
-- Create Date : 28/10/2014 17:13:59

local libEmoticons = LibStub:GetLibrary ("gxchat-LibEmoticons");

local tabEmoticons = {
	[":)"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_1.blp",
	[":-)"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_1.blp",
	["]:)"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_2.blp",
	["^_^"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_3.blp",
	["^^"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_3.blp",
	["^-^"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_3.blp",
	["^.^"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_3.blp",
	[":'-("] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["_("] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	[":'["] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["='("] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["='["] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	[":'-<"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	[":'<"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["='<"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["T_T"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["T.T"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["y_y"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["y.y"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["�_�"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["�.�"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_4.blp",
	["XD"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_5.blp",
	[":P"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_6.blp",
	[":p"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_6.blp",
	[";)"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_7.blp",
	[":O"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_8.blp",
	[":o"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_8.blp",
	[":|"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_9.blp",
	[":||"] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_9.blp",
	[":("] = "Interface\\AddOns\\gxchat_emoticons_standard_pack\\Immagini\\smiley_10.blp",
}

libEmoticons:AddEmoticonsFromTable ("standard", tabEmoticons);